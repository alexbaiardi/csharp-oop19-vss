﻿namespace csharp_oop19_VSS.model.people
{

    /// <summary>
    /// Represent the possibly status of a person.
    /// </summary>
    public enum Status
    {
        SUSCEPTIBLE,
        INFECTED,
        ILL,
        RECOVERED
    }
}
