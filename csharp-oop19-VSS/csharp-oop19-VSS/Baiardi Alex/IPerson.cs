﻿using csharp_oop19_VSS.model.virus;

namespace csharp_oop19_VSS.model.people
{

    /// <summary>
    /// Represents a Person.
    /// </summary>
    public interface IPerson
    {
        /// <summary>
        /// Represent the status of a people.
        /// </summary>
        Status Status { get; set; }

        /// <summary>
        /// Calculate if this person infect another person in contact with it.
        /// </summary>
        /// <returns>true: infection occurred
        ///          false: infection not occurred</returns>
        bool TryToInfect();

        /// <summary>
        /// Infects a person with a virus that have specific parameters for the person.
        /// </summary>
        /// <param name="virus">The instance of the virus that infected a person</param>
        void Infect(IVirus virus);

        /// <summary>
        /// Represent the virus that have infected a person or null if there isn't infection
        /// </summary>
        IVirus Virus { get; }

    }
}
