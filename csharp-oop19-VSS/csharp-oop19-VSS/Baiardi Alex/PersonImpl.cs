﻿using csharp_oop19_VSS.model.virus;
using System;

namespace csharp_oop19_VSS.model.people
{

    public class PersonImpl : IPerson
    {
        private IVirus virus;

        public IVirus Virus
        {
            get
            {
                return virus;
            }
        }

        public Status Status { get; set; }

        /// <summary>
        /// Construct a Person with default value.
        /// </summary>
        public PersonImpl()
        {
            Status = Status.SUSCEPTIBLE;
            virus = null;
        }

        public void Infect(IVirus virus)
        {
            if (Status != Status.SUSCEPTIBLE)
            {
                throw new InvalidOperationException("Invalid person status");
            }
            this.virus = virus;
            Status=Status.INFECTED;
        }

        public bool TryToInfect()
        {
            bool res = false;
            if (Virus != null)
            {
                Random r = new Random();
                res = r.NextDouble() * 100 < Virus.Infectivity;
            }
            return res;
        }
    }
}
