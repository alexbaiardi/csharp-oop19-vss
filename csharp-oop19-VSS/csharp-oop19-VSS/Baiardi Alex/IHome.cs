﻿using csharp_oop19_VSS.model.people;

namespace csharp_oop19_VSS.model.places
{
    /// <summary>
    /// Interface that models the place where people retire.
    /// </summary>
    public interface IHome : IPlace
    {
        /// <summary>
        /// Check the presence of a Person in the home.
        /// </summary>
        /// <param name="person">The person to look for in the place.</param>
        /// <returns>true if the person is present. false if the person isn't present.</returns>
        bool CheckPresence(IPerson person);

        /// <summary>
        /// Percentage increment of time that people spend at home.
        /// </summary>
        double HomeTendencyIncrement { set; }
    }
}
