﻿using csharp_oop19_VSS.model.people;
using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp_oop19_VSS.model.places
{
    public abstract class AbstractPlace : IPlace
    {
        private readonly Dictionary<int, List<IPerson>> peopleMap = new Dictionary<int, List<IPerson>>();
        
        public virtual void Enter(IPerson person, int time)
        {
            Check(GetAllPeople().Contains(person));
            if (!peopleMap.ContainsKey(time))
            {
                peopleMap.Add(time, new List<IPerson>());
            }
            peopleMap[time].Add(person);  
        }

        public virtual List<IPerson> Exit(int time)
        {
            List<IPerson> exitPeople = new List<IPerson>();
            if (peopleMap.ContainsKey(time))
            {
                exitPeople = peopleMap[time];
                peopleMap.Remove(time);
            }
            return exitPeople;
        }

        public virtual void ExitSinglePerson(IPerson person)
        {
            Check(!this.GetAllPeople().Contains(person));
            peopleMap.Values.Where(x => x.Contains(person)).FirstOrDefault().Remove(person);
        }

        public List<IPerson> GetAllPeople()
        {
            return peopleMap.Values.SelectMany(x => x).ToList();
        }

        /// <summary>
        /// Removes all the occurrences in the people map.
        /// </summary>
        protected void RemoveAll()
        {
            peopleMap.Clear();
        }

        protected void Check(bool condition)
        {
            if (condition)
            {
                throw new InvalidOperationException();
            }
        }
    }
}
