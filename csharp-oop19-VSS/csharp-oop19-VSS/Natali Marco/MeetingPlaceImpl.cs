﻿using csharp_oop19_VSS.model.people;
using System;
using System.Collections.Generic;
using System.Linq;

namespace csharp_oop19_VSS.model.places
{
    public class MeetingPlaceImpl : AbstractPlace, IMeetingPlace
    {
        private const int MAX_TIME = 24;
        private const int MAX_CAPACITY = 20;
        private readonly List<IPerson> people; // list that stores susceptible people and those who can infect.

        /// <summary>
        /// Constructor method
        /// </summary>
        public MeetingPlaceImpl()
        {
            IsOpen = true;
            people = new List<IPerson>();
        }

        public bool IsOpen { get ; set; }

        public bool CheckPresence(IPerson person)
        {
            return GetAllPeople().Contains(person);
        }

        public bool IsFull()
        {
            return GetAllPeople().Count == MAX_CAPACITY;
        }

        public void Open()
        {
            IsOpen = true;
        }

        public List<IPerson> Close()
        {
            IsOpen = false;
            List<IPerson> allPeople = GetAllPeople();
            RemoveAll();
            people.Clear();
            return allPeople;
        }

        public List<IPerson> EnterAndInfect(IPerson person, int time)
        {
            int exitTime = time + CalculateTime();
            Enter(person, exitTime );
            return person.Status == Status.INFECTED ? InfectOther(person) 
                    : person.Status == Status.SUSCEPTIBLE ? InfectedBy(person) : new List<IPerson>();
        }

        /// <summary>
        /// An infected person who enters the place infects susceptible people.
        /// </summary>
        /// <param name="person">infected person</param>
        /// <returns>list of new-infceted people</returns>
        private List<IPerson> InfectOther(IPerson person)
        {
            List<IPerson> infected = new List<IPerson>();
            people.Add(person);
            foreach (IPerson p in people.Where(p=>p.Status == Status.SUSCEPTIBLE)
                .Where(p=>person.TryToInfect()).ToList())
            {
                infected.Add(p);
                people.Remove(p);
            }
            infected.ForEach(p=>p.Infect(person.Virus.Duplicate()));
            return infected;
            
        }

        /// <summary>
        /// A susceptible person who enters the place is infected by people inside.
        /// </summary>
        /// <param name="person">the suceptible person</param>
        /// <returns>the person or an empty list if the person doesn't get the virus</returns>
        private List<IPerson> InfectedBy(IPerson person)
        {
            List<IPerson> infected = new List<IPerson>();
            IPerson p = people.Where(per => per.Status == Status.INFECTED).FirstOrDefault(per => per.TryToInfect());
            if(p != null)
            {
                person.Infect(p.Virus.Duplicate());
                infected.Add(person);
            }
            else
            {
                people.Add(person);
            }
            return infected;
        }

        public override List<IPerson> Exit(int time)
        {
            List<IPerson> exitPeople = base.Exit(time);
            people.RemoveAll(p=>exitPeople.Contains(p));
            return exitPeople;
        }

        public override void ExitSinglePerson(IPerson person)
        {
            base.ExitSinglePerson(person);
            people.Remove(person);
        }

        private int CalculateTime()
        {
            Random r = new Random();
            return r.Next(MAX_TIME);
        }
    }
}
