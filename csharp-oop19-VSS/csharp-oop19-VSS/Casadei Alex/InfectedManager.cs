﻿using csharp_oop19_VSS.controller;
using csharp_oop19_VSS.model.people;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_oop19_VSS.controller
{
    public class InfectedManager : IInfectedManager
    {
        private readonly Dictionary<int, List<IPerson>> infectedPeople;

        public InfectedManager()
        {
            this.infectedPeople = new Dictionary<int, List<IPerson>>();
        }
        public void AddInfected(int instant, IPerson person)
        {
            List<IPerson> temp = new List<IPerson>();
            if (infectedPeople.ContainsKey(instant))
            {
                temp = infectedPeople[instant];
            }
            int time = instant + person.Virus.IncubationPeriod;
            if (!infectedPeople.ContainsKey(time))
            {
                temp.Add(person);
                infectedPeople.Add(time, temp);
            }
        }

        public List<IPerson> ChangeStatus(int instant)
        {
            if (infectedPeople.ContainsKey(instant))
            {
                List<IPerson> temp = infectedPeople[instant];
                foreach (IPerson p in temp)
                {
                    p.Status = Status.ILL;
                }
                infectedPeople.Remove(instant);
                return temp;
            }
            return new List<IPerson>();
        }

        public bool IsAnyoneInfected()
        {
            return infectedPeople.Any();
        }
    }
}
