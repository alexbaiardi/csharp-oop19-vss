﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using csharp_oop19_VSS.model.people;

namespace csharp_oop19_VSS.controller
{
    public interface IPeopleManager
    {
        /// <summary>
        /// Generate a list of person who will born.
        /// </summary>
        /// <param name="totPerson"> total number of person in the simulation </param>
        /// <returns> list of person who born </returns>
        List<IPerson> Birth(int totPerson);

        /// <summary>
        /// Generates the list of person who will die.
        /// </summary>
        /// <param name="list"> list of people outside </param>
        /// <returns> list of dead person </returns>
        List<IPerson> Death(List<IPerson> list);

        /// <summary>
        /// Generate the list of person who will go to home.
        /// </summary>
        /// <param name="list"> list of people outside </param>
        /// <returns> list of person who will go to home </returns>
        List<IPerson> GoHome(List<IPerson> list);

        /// <summary>
        /// set the increment of the home tendency.
        /// </summary>
        /// <param name="homeTendencyIncrement"> value of the increment </param>
        double SetHomeTendencyIncrement { set; }
    }
}