﻿using csharp_oop19_VSS.model.people;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_oop19_VSS.controller
{
    public interface IInfectedManager
    {
        /// <summary>
        /// Stores information about a new infected people.
        /// </summary>
        /// <param name="instant">the instant when the person become infected</param>
        /// <param name="person">the person that contracts the infection</param>
        void AddInfected(int instant, IPerson person);

        /// <summary>
        /// Checks if in the specified instant the people have terminate the incubation period and change the status to Ill.
        /// </summary>
        /// <param name="instant"> nt the instant where execute the operations</param>
        /// <returns> list of ILL people</returns>
        List<IPerson> ChangeStatus(int instant);

        /// <summary>
        /// Checks if there are infected people.
        /// </summary>
        /// <returns>True if one or more people are infected False if nobody is infected</returns>
        bool IsAnyoneInfected();

    }

}

