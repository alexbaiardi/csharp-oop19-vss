﻿using System.Collections.Generic;


namespace csharp_oop19_VSS.model.virus
{
    public interface IVirus
    {
        double Infectivity { get; }

        double Mortality { get; }

        int IncubationPeriod { get; }

        int RecoveryPeriod { get; }

        IVirus Duplicate();
    }
}
