﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_oop19_VSS.model.virus
{
    public class VirusFactoryImpl : IVirusFactory
    {
        private int minIncubationPeriod;
        private int maxIncubationPeriod;
        private double infectivity;
        private double mortality;
        private int minRecoveryPeriod;
        private int maxRecoveryPeriod;


        public VirusFactoryImpl( int minIncubationPeriod, int maxIncubationPeriod, double infectifity,
            double mortality, int minRecoveryPeriod, int maxRecoveryPeriod)
        {
            this.minIncubationPeriod = minIncubationPeriod;
            this.maxIncubationPeriod = maxIncubationPeriod;
            this.infectivity = infectifity;
            this.mortality = mortality;
            this.minRecoveryPeriod = minRecoveryPeriod;
            this.maxRecoveryPeriod = maxRecoveryPeriod;
        }

        public IVirus CreateVirus()
        {
            var rnd = new Random();
            int recoveryPeriod = minRecoveryPeriod + rnd.Next(maxRecoveryPeriod - minRecoveryPeriod + 1);
            int incubationPeriod = minIncubationPeriod + rnd.Next(maxIncubationPeriod - minIncubationPeriod + 1);
            return new VirusImpl(incubationPeriod, infectivity, mortality, recoveryPeriod, minIncubationPeriod, maxIncubationPeriod,
                minRecoveryPeriod, maxRecoveryPeriod);
        }
    }
}
