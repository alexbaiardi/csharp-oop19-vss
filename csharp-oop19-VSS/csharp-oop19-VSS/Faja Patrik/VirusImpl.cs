﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_oop19_VSS.model.virus
{
    public class VirusImpl : IVirus
    {
        private const int QUANTUM_IN_A_DAY = 24;
        protected int incubationPeriod;
        protected int recoveryPeriod;
        protected double infectivity;
        protected double mortality;
        private int minIncubationPeriod;
        private int maxIncubationPeriod;
        private int minRecoveryPeriod;
        private int maxRecoveryPeriod;

        public int IncubationPeriod
        {
            get { return this.incubationPeriod*QUANTUM_IN_A_DAY; }
        }

        public double Infectivity
        {
            get { return this.infectivity; }
        }

        public double Mortality
        {
            get { return this.mortality; }
        }

        public int RecoveryPeriod
        {
            get { return this.recoveryPeriod*QUANTUM_IN_A_DAY; }
        }

        public VirusImpl(int incubationPeriod, double infectivity, 
            double mortality, int recoveryPeriod, int minIncubationPeriod, 
            int maxIncubationPeriod, int minRecoveryPeriod, int maxRecoveryPeriod)
        {
            this.incubationPeriod = incubationPeriod;
            this.recoveryPeriod = recoveryPeriod;
            this.mortality = mortality;
            this.infectivity = infectivity;
            this.minIncubationPeriod = minIncubationPeriod;
            this.minRecoveryPeriod = minRecoveryPeriod;
            this.maxIncubationPeriod = maxIncubationPeriod;
            this.maxRecoveryPeriod = maxRecoveryPeriod;
        }

        public IVirus Duplicate()
        {
            var factory = new VirusFactoryImpl(minIncubationPeriod, maxIncubationPeriod, infectivity,
                mortality, minRecoveryPeriod, maxRecoveryPeriod);
            return factory.CreateVirus();
        }
    }
}
