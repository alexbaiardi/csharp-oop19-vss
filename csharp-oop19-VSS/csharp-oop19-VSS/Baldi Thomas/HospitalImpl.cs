﻿using csharp_oop19_VSS.model.people;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_oop19_VSS.model.places
{
    public class HospitalImpl : AbstractPlace, IHospital
    {
        
        public override void Enter(IPerson person, int time)
        {
            if (person.Virus==null)
            {
                throw new ArgumentException();
            }
            base.Enter(person, time + person.Virus.RecoveryPeriod);
        }

        public IHospitalizationOutcome ExitWithOutcome(int time)
        {
            List<IPerson> people = base.Exit(time);
            List<IPerson> healed = people.Where(x => Outcome(x)).ToList();
            healed.ForEach(p => p.Status = Status.RECOVERED);

            healed.ForEach(p => people.Remove(p));
            return new HospitalizationOutcomeImpl(people, healed);
        }

        /// <summary>
        /// that calculates if a person is dead or recovered.
        /// </summary>
        /// <param name="person">person The person to be analyzed.</param>
        /// <returns>true if a person is recovered </returns>
        /// <returns>false if a person is dead</returns>
        private bool Outcome(IPerson person)
        {
            Random rand = new Random();
            return rand.Next(100) > person.Virus.Mortality;
        }

        public bool IsAnyoneInHospital()
        {
            return base.GetAllPeople().Any();
        }
        
    }
}
