﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csharp_oop19_VSS.model.people;

namespace csharp_oop19_VSS.model.places
{
    public class HospitalizationOutcomeImpl : IHospitalizationOutcome
    {
        private List<IPerson> deadPeople;
        private List<IPerson> recoveredPeople;

        /// <summary>
        /// Constructor method for the hospitalization outcome.
        /// </summary>
        public HospitalizationOutcomeImpl(List<IPerson> death, List<IPerson> recovered)
        {
            this.deadPeople = death;
            this.recoveredPeople = recovered;
        }

        public List<IPerson> GetDeadPeople
        {
            get
            {
                return deadPeople;
            }
        }

        public List<IPerson> GetRecoveredPeople
        {
            get
            {
                return recoveredPeople;
            }
        }

    }
}
