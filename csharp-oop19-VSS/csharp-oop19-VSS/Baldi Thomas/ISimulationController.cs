﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace csharp_oop19_VSS.Baldi_Thomas
{
    public interface ISimulationController
    {
        /// <summary>
        /// Method to start the simulation.
        /// </summary>
        void Start();

        /// <summary>
        /// Method to stop the simulation.
        /// </summary>
        void Stop();

        /// <summary>
        /// Method to start a new simulation.
        /// </summary>
        void NewSimulation();

        /// <summary>
        /// Method to set the alert state.
        /// </summary>
        /// <param name="homeTendency">Percentage increment of time that people spend at home</param>
        void NotifyAlert(double homeTendency);

        /// <summary>
        /// Method to unset the alert state.
        /// </summary>
        void NotyfyAlertOff();

        /// <summary>
        /// Method to change the simulation speed.
        /// </summary>
        void SetSimulationSpeed(int speed);

        /// <summary>
        /// Method that gets virus manager.
        /// </summary>
        /// <returns>virus manager</returns>
        //VirusManager GetVirusManager();

        /// <summary>
        /// Method for return the size of the map.
        /// </summary>
        /// <returns>mapSize</returns>
        double GetMapsize();
    }
}
