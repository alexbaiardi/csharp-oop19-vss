﻿using System;
using System.Text;
using System.Collections.Generic;
using csharp_oop19_VSS.controller;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using csharp_oop19_VSS.model.people;

namespace UnitTest.Alex_Casadei
{
    /// <summary>
    /// Descrizione del riepilogo per TestPeopleManager
    /// </summary>
    [TestClass]
    public class TestPeopleManager
    {
        private IPeopleManager pm = new PeopleManager(0.2,0.8,12.5);

   

        #region Attributi di test aggiuntivi
        //
        // È possibile utilizzare i seguenti attributi aggiuntivi per la scrittura dei test:
        //
        // Utilizzare ClassInitialize per eseguire il codice prima di eseguire il primo test della classe
        // [ClassInitialize()]
        // public static void MyClassInitialize(TestContext testContext) { }
        //
        // Utilizzare ClassCleanup per eseguire il codice dopo l'esecuzione di tutti i test della classe
        // [ClassCleanup()]
        // public static void MyClassCleanup() { }
        //
        // Utilizzare TestInitialize per eseguire il codice prima di eseguire ciascun test 
        // [TestInitialize()]
        // public void MyTestInitialize() { }
        //
        // Utilizzare TestCleanup per eseguire il codice dopo l'esecuzione di ciascun test
        // [TestCleanup()]
        // public void MyTestCleanup() { }
        //
        #endregion

        [TestMethod]
        public void TestDeath()
        {
            Random r = new Random();
            List<IPerson> list = new List<IPerson>();
            IPerson p = new PersonImpl();
            p.Status = Status.INFECTED;
            for (int i=0; i<10; i++)
            {
                list.Add(p);
            }
            Assert.IsTrue(pm.Death(list).Count==0);

            for(int i=0;i<50;i++)
            {
                if(r.Next(2) == 1) 
                {
                    list.Add(p);
                } 
                else
                {
                    list.Add(new PersonImpl());
                }
                
            }

            List<IPerson> returnList = new List<IPerson>();
            returnList = pm.Death(list);
            Assert.IsTrue(returnList.Count < list.Count);
            returnList.ForEach(per => Assert.IsTrue(list.Contains(per)));
        }

        [TestMethod]
        public void TestBirth()
        {
            int tot= 50; // number of person
            List<IPerson> list = new List<IPerson>();
            list = pm.Birth(tot);
            Assert.IsNotNull(list);
            Assert.IsTrue(list.Count < tot);


        }
    }
}
