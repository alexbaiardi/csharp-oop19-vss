﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using csharp_oop19_VSS.model.virus;

namespace UnitTest.virus
{
    [TestClass]
    public class VirusTest
    {
        private static int QUANTIUM_IN_A_DAY = 24;
        private IVirus virus;
        private int incubationPeriodMin;
        private int incubationPeriodmax;
        private double mortality = 2;
        private double infectivity = 3;
        private int recoveryPeriodmin = 2;
        private int recoveryPeriodmax = 10;

        [TestMethod]
        public void TestVirusValue()
        {
            int incubationPeriod = virus.IncubationPeriod;
            int recoveryPeriod = virus.RecoveryPeriod;
            Assert.AreEqual(virus.Infectivity, infectivity);
            Assert.AreEqual(virus.Mortality, mortality);
            Assert.IsTrue(incubationPeriod >= incubationPeriodMin * QUANTIUM_IN_A_DAY && incubationPeriod <= incubationPeriodmax * QUANTIUM_IN_A_DAY);
            Assert.IsTrue(recoveryPeriod >= recoveryPeriodmin * QUANTIUM_IN_A_DAY && recoveryPeriod <= recoveryPeriodmax * QUANTIUM_IN_A_DAY);
            for (int i = 0; i < 10; i++)
            {
                Assert.AreEqual(recoveryPeriod, virus.RecoveryPeriod);
                Assert.AreEqual(incubationPeriod, virus.IncubationPeriod);
            }
        }

        [TestInitialize]
        public void InizializeVirus()
        {
            this.incubationPeriodMin = 1;
            this.incubationPeriodmax = 10;
            this.mortality = 2;
            this.infectivity = 3;
            this.recoveryPeriodmin = 2;
            this.recoveryPeriodmax = 10;
            virus = new VirusFactoryImpl(incubationPeriodMin, incubationPeriodmax, infectivity,
                      mortality, recoveryPeriodmin, recoveryPeriodmax).CreateVirus();
        }

    }
}
