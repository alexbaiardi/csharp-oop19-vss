﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using csharp_oop19_VSS.model.places;
using System.Collections.Generic;
using csharp_oop19_VSS.model.people;

namespace UnitTest.Baiardi_Alex
{
    /// <summary>
    /// Home testing class.
    /// </summary>
    [TestClass]
    public class TestHome
    {
        private readonly IHome home = new HomeImpl(0);

        /// <summary>
        /// Tests the entry method.
        /// </summary>
        [TestMethod]
        [ExpectedException(typeof(InvalidOperationException))]
        public void TestEntry()
        {
            int nPeople = 20;
            List<IPerson> people = new List<IPerson>();
            for(int i = 0; i < nPeople; i++)
            {
                IPerson p = new PersonImpl();
                home.Enter(p, 1);
                Assert.IsTrue(home.CheckPresence(p));
            }
            IPerson person = new PersonImpl();
            home.Enter(person, 2);
            Assert.IsTrue(home.CheckPresence(person));
            home.Enter(person, 4);
            Assert.Fail("A person who entered is already inside");
        }

        /// <summary>
        /// Tests the exit method.
        /// </summary>
        [TestMethod]
        public void TestExit()
        {
            int nPeople = 20;
            List<IPerson> people = new List<IPerson>();
            for (int i = 0; i < nPeople; i++)
            {
                IPerson p = new PersonImpl();
                home.Enter(p, 1);
            }
            home.Exit(1);
            people.ForEach(p => Assert.IsFalse(home.CheckPresence(p)));

            IPerson person = new PersonImpl();
            home.Enter(person, 2);
            home.Exit(2);
            Assert.IsFalse(home.CheckPresence(person));
            Assert.IsTrue(home.Exit(2).Count==0);
        }

        /// <summary>
        /// Tests the checkPresence method.
        /// </summary>
        [TestMethod]
        public void TestCheckPresence()
        {
            IPerson person = new PersonImpl();
            home.Enter(person, 1);
            Assert.IsTrue(home.CheckPresence(person));
            home.Exit(1);
            Assert.IsFalse(home.CheckPresence(person));
        }
    }
}
