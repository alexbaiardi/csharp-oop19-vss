﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using csharp_oop19_VSS.model.people;
using csharp_oop19_VSS.model.virus;

namespace UnitTest.person
{
    /// <summary>
    /// Check the correct behaviour of Person object
    /// </summary>
    [TestClass]
    public class TestPerson
    {
        [TestMethod]
        public void TestInfect()
        {
            IPerson p = new PersonImpl();
            Assert.AreEqual(Status.SUSCEPTIBLE,p.Status);
            Assert.IsNull(p.Virus);
            IVirusFactory vf = new VirusFactoryImpl(1, 2, 8, 10, 2, 10);
            IVirus v = vf.CreateVirus();
            p.Infect(v);
            Assert.AreEqual(Status.INFECTED, p.Status);
            try
            {
                p.Infect(v);
                Assert.Fail("An INFECTED person has become infected twice");
            }
            catch(InvalidOperationException ex) { }
            try
            {
                p.Status = Status.ILL;
                p.Infect(v);
                Assert.Fail("An ILL person has become infected another times");
            }
            catch (InvalidOperationException ex) { }
            try
            {
                p.Status = Status.RECOVERED;
                p.Infect(v);
                Assert.Fail("An RECOVERED person has become infected another times");
            }
            catch (InvalidOperationException ex) { }
        }

        [TestMethod]
        public void TestTrytoInfect()
        {
            IPerson p = new PersonImpl();
            Assert.IsFalse(p.TryToInfect());

            IVirusFactory vf=new VirusFactoryImpl(1, 2, 50, 10, 2, 10);
            p.Infect(vf.CreateVirus());
            for(int i=0; i < 10; i++)
            {
                Console.WriteLine(p.TryToInfect());//Print some values to test randomness
            }

            IPerson p1 = new PersonImpl();
            Assert.IsFalse(p1.TryToInfect());

            IVirusFactory infectiveVirus = new VirusFactoryImpl(1, 2, 100, 10, 2, 10);
            p1.Infect(infectiveVirus.CreateVirus());
            for (int i = 0; i < 10; i++)
            {
                Assert.IsTrue(p1.TryToInfect());
            }
        }
    }
}
